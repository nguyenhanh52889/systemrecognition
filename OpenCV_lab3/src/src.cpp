#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <array>
#include <cmath>

using namespace cv;
using namespace std;
cv::Mat img;

cv::Mat DrawHistogram(cv::Mat matInput, bool drawOnInputMat)
{
    if (!matInput.data)
    {
        std::cout << "image input error";
        return cv::Mat();
    }

    std::vector<cv::Mat> bgr_planes;
    cv::split(matInput, bgr_planes);

    /// Establish the number of bins
    int histSize = 256;

    /// Set the ranges ( for B,G,R) )
    float range[] = { 0, 256 };
    const float* histRange = { range };

    bool uniform = true;
    bool accumulate = false;

    cv::Mat b_hist, g_hist, r_hist;

    /// Compute the histograms:
    cv::calcHist(&bgr_planes[0], 1, 0, cv::Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate);
    if (matInput.channels() == 3)
    {
        cv::calcHist(&bgr_planes[1], 1, 0, cv::Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate);
        cv::calcHist(&bgr_planes[2], 1, 0, cv::Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate);
    }


    // Draw the histograms for B, G and R
    int hist_w = matInput.cols;
    int hist_h = matInput.rows;
    double bin_w = (double)hist_w / histSize;

    cv::Mat histImage(hist_h, hist_w, CV_8UC3, cv::Scalar(0, 0, 0));
    if (drawOnInputMat)
    {
        histImage = matInput.clone();
        if (histImage.channels() == 1)
        {
            cv::cvtColor(histImage, histImage, 8);
        }
    }

    /// Normalize the result to [ 0, histImage.rows ]
    cv::normalize(b_hist, b_hist, 0, histImage.rows, cv::NORM_MINMAX, -1, cv::Mat());
    cv::normalize(g_hist, g_hist, 0, histImage.rows, cv::NORM_MINMAX, -1, cv::Mat());
    cv::normalize(r_hist, r_hist, 0, histImage.rows, cv::NORM_MINMAX, -1, cv::Mat());

    int thickness = 1;
    /// Draw for each channel
    for (int i = 1; i < histSize; i++)
    {
        cv::line(histImage, cv::Point(bin_w * (i - 1), hist_h - cvRound(b_hist.at<float>(i - 1))),
            cv::Point(bin_w * (i), hist_h - cvRound(b_hist.at<float>(i))),
            matInput.channels() == 3 ? cv::Scalar(255, 0, 0) : cv::Scalar(255, 255, 255), thickness, 8, 0);
        if (matInput.channels() == 3)
        {
            cv::line(histImage, cv::Point(bin_w * (i - 1), hist_h - cvRound(g_hist.at<float>(i - 1))),
                cv::Point(bin_w * (i), hist_h - cvRound(g_hist.at<float>(i))),
                cv::Scalar(0, 255, 0), thickness, 8, 0);
            cv::line(histImage, cv::Point(bin_w * (i - 1), hist_h - cvRound(r_hist.at<float>(i - 1))),
                cv::Point(bin_w * (i), hist_h - cvRound(r_hist.at<float>(i))),
                cv::Scalar(0, 0, 255), thickness, 8, 0);
        }
    }

    return histImage;
}

Mat Binary_threshold(cv::Mat3b img) {
    int width = img.cols;
    int height = img.rows;
    Mat res(height, width, CV_8UC1);
    const int S = width / 8;
    int s2 = S / 2;
    const float t = 0.15;
    unsigned long* integral_image = 0;
    integral_image = new unsigned long[width * height * sizeof(unsigned long*)];
    long sum = 0;
    int count = 0;
    int index;
    int x1, y1, x2, y2;

    //рассчитываем интегральное изображение 


    for (int i = 0; i < width; i++) {
        sum = 0;
        for (int j = 0; j < height; j++) {
            index = j * width + i;
            sum += (0.2121 * img.at<Vec3b>(j, i)[2] + 0.7154 * 
                img.at<Vec3b>(j, i)[1] + 0.0721 * img.at<Vec3b>(j, i)[0]);
            if (i == 0 || j == 0)
                integral_image[index] = sum;
            else
                integral_image[index] = integral_image[index - 1] + sum;
        }
    }

    //находим границы для локальные областей
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            index = j * width + i;

            x1 = i - s2;
            x2 = i + s2;
            y1 = j - s2;
            y2 = j + s2;

            if (x1 < 0)
                x1 = 0;
            if (x2 >= width)
                x2 = width - 1;
            if (y1 < 0)
                y1 = 0;
            if (y2 >= height)
                y2 = height - 1;

            count = (x2 - x1) * (y2 - y1);

            sum = integral_image[y2 * width + x2] - integral_image[y1 * width + x2] -
                integral_image[y2 * width + x1] + integral_image[y1 * width + x1];

            if ((long)((0.2121 * img.at<Vec3b>(j, i)[2] + 0.7154 *
                img.at<Vec3b>(j, i)[1] + 0.0721 * img.at<Vec3b>(j, i)[0])
                * count) < (long)(sum * (1.0 - t)))
                res.at<uint8_t>(j, i) = 0;
            else
                res.at<uint8_t>(j, i) = 255;
        }
    }

    return res;
}

Mat conv(Mat3b img, Mat3b kernel)
{
    int width = img.cols;
    int height = img.rows;
    Mat res(height, width, CV_8UC1);

    for (int i = 1; i < width-1; i++) {
        for (int j = 1; j < height-1; j++) {
            for (int k = 0; k < 3; k++) {
                res.at<uint8_t>(j, i) += (kernel.at<Vec3b>(0, 0)[k] * img.at<Vec3b>(j-1, i-1)[k] + kernel.at<Vec3b>(0, 1)[k]
                    * img.at<Vec3b>(j-1, i)[k] + kernel.at<Vec3b>(0, 2)[k] * img.at<Vec3b>(j-1, i+1)[k]) +
                    (kernel.at<Vec3b>(1, 0)[k] * img.at<Vec3b>(j, i-1)[k] + kernel.at<Vec3b>(1, 1)[k] *
                        img.at<Vec3b>(j, i)[k] + kernel.at<Vec3b>(1, 2)[k] * img.at<Vec3b>(j, i+1)[k]) +
                        (kernel.at<Vec3b>(2, 0)[k] * img.at<Vec3b>(j+1, i-1)[k] + kernel.at<Vec3b>(2, 1)[k] *
                            img.at<Vec3b>(j+1, i)[k] + kernel.at<Vec3b>(2, 2)[k] * img.at<Vec3b>(j+1, i+1)[k]);


            }
            if (res.at<uint8_t>(j, i) < 0.5 * 256) 
                res.at<uint8_t>(j, i) = 0;
            else
                res.at<uint8_t>(j, i) = 255;
        }
    }
    return res;
}

Mat3b local(cv::Mat3b img)
{
    int width = img.cols;
    int height = img.rows;
    Mat res(height, width, CV_8UC1);
    int R;
    int M;
    int S;
    int B;
    float k;

    int l;
    int r;
    int u;
    int d;

    k = 0.2;
    for (int i = 1; i < width; i++)
    {
        for (int j = 1; j < height; j++)
        {
            R = img.at<Vec3b>(j, i)[0] + img.at<Vec3b>(j, i)[1] + img.at<Vec3b>(j, i)[2];


            l = 0.0721 * img.at<Vec3b>(j - 1, i)[0] + 0.7154 * img.at<Vec3b>(j - 1, i)[1] + 0.2121 * img.at<Vec3b>(j - 1, i)[2];
            r = 0.0721 * img.at<Vec3b>(j + 1, i)[0] + 0.7154 * img.at<Vec3b>(j + 1, i)[1] + 0.2121 * img.at<Vec3b>(j + 1, i)[2];
            d = 0.0721 * img.at<Vec3b>(j, i - 1)[0] + 0.7154 * img.at<Vec3b>(j, i - 1)[1] + 0.2121 * img.at<Vec3b>(j, i - 1)[2];
            u = 0.0721 * img.at<Vec3b>(j, i + 1)[0] + 0.7154 * img.at<Vec3b>(j, i + 1)[1] + 0.2121 * img.at<Vec3b>(j, i + 1)[2];

            M = l + r + u + d;

            S = 0.5 * sqrt(pow((R - l), 2) + pow((R - r), 2) + pow((R - u), 2) + pow((R - d), 2));

            B = M + k * S;

            if (R > B)
            {
                img.at<Vec3b>(j, i) = 0;
            }
            /*else
            {
                res.at<Vec3b>(j, i) = 255;
            }*/

        }
    }
    return img;
}

void local_threshold(const cv::Mat& src, cv::Mat& out)
{

    out = cv::Mat(src.rows, src.cols, CV_8UC1);
    double maxVal, minVal;

    int top, bottom, left, right;
    int borderType = cv::BORDER_CONSTANT;
    cv::Scalar value;
    top = (int)(4); bottom = (int)(4);
    left = (int)(4); right = (int)(4);
    Mat output = src;
    value = 0;
    cv::copyMakeBorder(src, output, top, bottom, left, right, borderType, value);

    for (int y = 4; y < output.rows - 4; y++) {

        for (int x = 4; x < output.cols - 4; x++) {

            // apply local ROI

            cv::Mat ROI = output(cv::Rect(cv::Point(x - 4, y - 4), cv::Size(9, 9)));
            cv::minMaxLoc(ROI, &minVal, &maxVal);    // extract max intensity values in the ROI

            if (src.at<uchar>(cv::Point(x - 4, y - 4)) >= 0.7 * maxVal) {    // apply local threshold w.r.t highest intensity level

                out.at<uchar>(cv::Point(x - 4, y - 4)) = 255;            // change pixel value in mask if true
            }
            else {
                out.at<uchar>(cv::Point(x - 4, y - 4)) = 0;

            }

        }
    }



}

int main()
{
    cv::Mat bgr_image = cv::imread("image.jpg");
    array<uint8_t, 256> lut_sqrt;
    for (int i = 0; i < lut_sqrt.size(); i++)
    {
        lut_sqrt[i] = sqrt(i);
    }
    Mat graphic(500, 500, CV_8UC1);
    for (int i = 0; i < 255; i++) {
        line(graphic, Point(i, 500 - lut_sqrt[i]), Point(i + 1, 500 - lut_sqrt[i + 1]), Scalar(0, 0, 0));
    }
    imshow("graphics", graphic);
    Mat src_lut;
    cv::LUT(bgr_image, lut_sqrt, src_lut);
    cv::Mat his_after_lut = DrawHistogram(src_lut, true);
    imshow("After lut", his_after_lut);

    Mat img = cv::imread("three.jpeg", CV_8UC1);
    Mat3b kernel(3, 3, Vec3b(0, 0, 0));
    kernel.at<Vec3b>(0,0) = Vec3b(-1, -1, -1);
    kernel.at<Vec3b>(0, 1) = Vec3b(-1, -1, -1);
    kernel.at<Vec3b>(0, 2) = Vec3b(-1, -1, -1);
    kernel.at<Vec3b>(1, 0) = Vec3b(-1, -1, -1);
    kernel.at<Vec3b>(1, 1) = Vec3b(8, 8, 8);
    kernel.at<Vec3b>(1, 2) = Vec3b(-1, -1, -1);
    kernel.at<Vec3b>(2, 0) = Vec3b(-1, -1, -1);
    kernel.at<Vec3b>(2, 1) = Vec3b(-1, -1, -1);
    kernel.at<Vec3b>(2, 2) = Vec3b(-1, -1, -1);
    Mat res1 = conv(img, kernel);
    Mat res2 = Binary_threshold(img);

    int width = img.cols;
    int height = img.rows;
    Mat res3;
    local_threshold(img, res3);
	cv::imshow("Example", res3);



    
    cv::Mat lab_image;
    cv::cvtColor(bgr_image, lab_image, 0);

    // Extract the L channel
    std::vector<cv::Mat> lab_planes(3);
    cv::split(lab_image, lab_planes);  // now we have the L image in lab_planes[0]

    // apply the CLAHE algorithm to the L channel
    cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
    clahe->setClipLimit(4);
    cv::Mat dst;
    clahe->apply(lab_planes[0], dst);

    // Merge the the color planes back into an Lab image
    dst.copyTo(lab_planes[0]);
    cv::merge(lab_planes, lab_image);

    // convert back to RGB
    cv::Mat image_clahe;
    cv::cvtColor(lab_image, image_clahe, 0);
    cv::Mat his = DrawHistogram(image_clahe, true);
    imshow("histogram CLAHE", his);
    // display the results  (you might also want to see lab_planes[0] before and after).
    cv::imshow("image original", bgr_image);
    //cv::imshow("image CLAHE", image_clahe);
	cv::waitKey(0);

    return 0;
}