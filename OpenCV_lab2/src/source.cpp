#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <array>

using namespace cv;
using namespace std;

cv::Mat DrawHistogram(cv::Mat matInput, bool drawOnInputMat)
{
	if (!matInput.data)
	{
		std::cout << "image input error";
		return cv::Mat();
	}

	std::vector<cv::Mat> bgr_planes;
	cv::split(matInput, bgr_planes);

	/// Establish the number of bins
	int histSize = 256;

	/// Set the ranges ( for B,G,R) )
	float range[] = { 0, 256 };
	const float* histRange = { range };

	bool uniform = true;
	bool accumulate = false;

	cv::Mat b_hist, g_hist, r_hist;

	/// Compute the histograms:
	cv::calcHist(&bgr_planes[0], 1, 0, cv::Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate);
	if (matInput.channels() == 3)
	{
		cv::calcHist(&bgr_planes[1], 1, 0, cv::Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate);
		cv::calcHist(&bgr_planes[2], 1, 0, cv::Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate);
	}


	// Draw the histograms for B, G and R
	int hist_w = matInput.cols;
	int hist_h = matInput.rows;
	double bin_w = (double)hist_w / histSize;

	cv::Mat histImage(hist_h, hist_w, CV_8UC3, cv::Scalar(0, 0, 0));
	if (drawOnInputMat)
	{
		histImage = matInput.clone();
		if (histImage.channels() == 1)
		{
			cv::cvtColor(histImage, histImage, 8);
		}
	}

	/// Normalize the result to [ 0, histImage.rows ]
	cv::normalize(b_hist, b_hist, 0, histImage.rows, cv::NORM_MINMAX, -1, cv::Mat());
	cv::normalize(g_hist, g_hist, 0, histImage.rows, cv::NORM_MINMAX, -1, cv::Mat());
	cv::normalize(r_hist, r_hist, 0, histImage.rows, cv::NORM_MINMAX, -1, cv::Mat());

	int thickness = 1;
	/// Draw for each channel
	for (int i = 1; i < histSize; i++)
	{
		cv::line(histImage, cv::Point(bin_w * (i - 1), hist_h - cvRound(b_hist.at<float>(i - 1))),
			cv::Point(bin_w * (i), hist_h - cvRound(b_hist.at<float>(i))),
			matInput.channels() == 3 ? cv::Scalar(255, 0, 0) : cv::Scalar(255, 255, 255), thickness, 8, 0);
		if (matInput.channels() == 3)
		{
			cv::line(histImage, cv::Point(bin_w * (i - 1), hist_h - cvRound(g_hist.at<float>(i - 1))),
				cv::Point(bin_w * (i), hist_h - cvRound(g_hist.at<float>(i))),
				cv::Scalar(0, 255, 0), thickness, 8, 0);
			cv::line(histImage, cv::Point(bin_w * (i - 1), hist_h - cvRound(r_hist.at<float>(i - 1))),
				cv::Point(bin_w * (i), hist_h - cvRound(r_hist.at<float>(i))),
				cv::Scalar(0, 0, 255), thickness, 8, 0);
		}
	}

	return histImage;
}


int main()
{
	Mat gradient(60, 768, CV_8UC1);

	for (int y = 0; y < 768; y += 3) {
		uint8_t val;
		val = y / 3;
		for (int x = 0; x < 60; x++) {
			for (int i = 0; i < 3; i++)
				gradient.at<uint8_t>(x, y + i) = val;
		}
	}

	Mat steps(60, 768, CV_8UC1);
	int color = 0;
	for (int y = 0; y < 768; y += 30) {
		uint8_t val = color;
		color += 10;
		for (int x = 0; x < 60; x++) {
			for (int i = 0; i < 30; i++) {
				if (y + i >= 768) {
					break;
				}
				steps.at<uint8_t>(x, y + i) = val;
			}
		}
	}

	// convertto gamma

	//Mat floatsteps = steps / 255.0f;
	Mat floatsteps;
	steps.convertTo(floatsteps, CV_32FC1, 1.0 / 255);
	Mat gamma;
	cv::pow(floatsteps, 2.1f, gamma);
	gamma.convertTo(gamma, CV_8UC1, 255.0);

	//lut
	array<uint8_t, 256> lut;

	for (int i = 0; i < lut.size(); i++)
	{
		lut[i] = cv::saturate_cast<uint8_t>(255 * pow(i / 255.0, 2.1f));
	}

	cv::Mat gamma_gradient;
	cv::LUT(gradient, lut, gamma_gradient);

	imshow("gradient gamma", gamma_gradient);
	imshow("gradient", gradient);
	imshow("steps", steps);
	imshow("gamma", gamma);

	Mat lab2(2 * 60, 2 * 768, CV_8UC1(0));
	gradient.copyTo(lab2(Rect(0, 0, gradient.cols, gradient.rows)));
	gamma_gradient.copyTo(lab2(Rect(0, gradient.rows, gamma_gradient.cols, gamma_gradient.rows)));
	steps.copyTo(lab2(Rect(steps.cols, 0, steps.cols, steps.rows)));
	gamma.copyTo(lab2(Rect(steps.cols, steps.rows, gamma.cols, gamma.rows)));
	imshow("lab2", lab2);
	vector<int> compression_params;
	compression_params.push_back(1);
	compression_params.push_back(65);

	imwrite("lab2_65.jpeg", lab2, compression_params);
	waitKey(0);
	return 0;
}